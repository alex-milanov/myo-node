
## Reference
- https://developerblog.myo.com/myo-bluetooth-spec-released/
- https://github.com/thalmiclabs/myo-bluetooth/blob/master/myohw.h - header file
- https://github.com/NiklasRosenstein/myo-python/blob/master/myo/libmyo.h
-

## Other
- https://support.getmyo.com/hc/en-us/articles/360018409792-Myo-Connect-SDK-and-firmware-downloads - downloads
- https://gist.github.com/indexzero/60d1224f0082f0d16f14 - myo bindings in node
