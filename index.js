const noble = require('noble');

noble.on('stateChange', function(state) {
	if (state === 'poweredOn') {
		console.log('Scanning for BLE devices...');
		noble.startScanning([], false);
	} else {
		noble.stopScanning();
	}
});

const readChar = (c, type = 'str') => c.read((err, data) => {
	console.log('char read', err, data);
	var buffer = new Buffer(data);
	switch (type) {
		case 'str':
			console.log(buffer.toString('utf-8'));
			break;
		case 'int':
			console.log(buffer[0]);
			break;
		default:
	}
});

const explore = p => {
	console.log(`Connecting to device ${p.advertisement.localName}`);
	p.on('disconnect', function() {
		// process.exit(0);
		noble.startScanning([], false);
	});

	p.connect(function(error) {
		console.log('Connected. \nDiscovering Services...');
		p.discoverServices([], (error, services) =>
			services.forEach(service => {
				console.log('Service', service.uuid, service.name);
				service.discoverCharacteristics([], (error, chars) => {
					chars.forEach(char => {
						console.log('Characteristic', char.uuid, char.name, service.uuid);
						if (char.uuid === '2a29') readChar(char);
						if (char.uuid === '2a19') readChar(char, 'int');
					});
				});
			})
		);
	});
};

noble.on('discover', function(p) {
	console.log('discovered #' + p.id + ': ' + p.advertisement.localName);
	console.log(JSON.stringify(p.advertisement, null, 2));
	if (p.id === '1872ae6290314fb481b3a084a2869a4d') explore(p);
	// if (re.test(peripheral.advertisement.localName) && peripheral.connectable) {
	//		 console.log('found connectable device.', peripheral.id)
	// }
});
